<?php

namespace App\DataFixtures;

use App\Factory\ApiTokenFactory;
use App\Factory\CommentFactory;
use App\Factory\LikeFactory;
use App\Factory\PostFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        UserFactory::createOne([
            'email' => 'bernie@dragonmail.com',
            'password' => 'roar',
        ]);

        $users = UserFactory::createMany(10);
        PostFactory::createMany(100, function () {
            return [
                'owner' => UserFactory::random(),
            ];
        });

        CommentFactory::createMany(4000, function () {
            return [
                'owner' => UserFactory::random(),
                'post' => PostFactory::random(),
            ];
        });

        $post = PostFactory::createOne();

        $comment1 = CommentFactory::createOne([
                'owner' => UserFactory::random(),
                'post' => $post,
            ]);

        $comment2 = CommentFactory::createOne([
            'owner' => UserFactory::random(),
            'post' => $post,
        ]);

        $comment3 = CommentFactory::createOne([
            'owner' => UserFactory::random(),
            'post' => $post,
        ]);

        $comment4 = CommentFactory::createOne([
            'owner' => UserFactory::random(),
            'post' => $post,
        ]);

        $comment5 = CommentFactory::createOne([
            'owner' => UserFactory::random(),
            'post' => $post,
        ]);

        foreach ($users as $user) {
            LikeFactory::createOne([
                'owner' => $user,
                'post' => $post,
            ]);

            LikeFactory::createOne([
                'owner' => $user,
                'comment' => $comment1,
            ]);

            LikeFactory::createOne([
                'owner' => $user,
                'comment' => $comment2,
            ]);

            LikeFactory::createOne([
                'owner' => $user,
                'comment' => $comment3,
            ]);

            LikeFactory::createOne([
                'owner' => $user,
                'comment' => $comment4,
            ]);

            LikeFactory::createOne([
                'owner' => $user,
                'comment' => $comment5,
            ]);
        }

        ApiTokenFactory::createMany(30, function () {
            return [
                'ownedBy' => UserFactory::random(),
            ];
        });
    }
}
