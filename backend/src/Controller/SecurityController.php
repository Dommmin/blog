<?php

namespace App\Controller;

use App\Entity\ApiToken;
use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SecurityController extends AbstractController
{
    #[Route('/login', name: 'app_login', methods: ['POST'])]
    public function login(EntityManagerInterface $entityManager, #[CurrentUser] $user = null): JsonResponse
    {
        if (!$user) {
            return $this->json([
                'error' => 'Invalid login request: check that the Content-Type header is "application/json".',
            ], 401);
        }

        $token = new ApiToken();
        $token->setOwnedBy($user);

        $entityManager->persist($token);
        $entityManager->flush();

        return $this->json([
            'access_token' => $token->getToken()
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new Exception('This should never be reached!');
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route('/user', name: 'app_user', methods: ['GET'])]
    public function user(NormalizerInterface $normalizer, #[CurrentUser] $user = null): JsonResponse
    {
        return $this->json([
            'user' => $normalizer->normalize($user, null, ['groups' => ['user:read']]),
        ]);
    }
}
