<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post as PostMetadata;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use App\Repository\PostRepository;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: PostRepository::class)]
#[ApiResource(
    types: ['https://schema.org/Book'],
    operations: [
        new GetCollection(),
        new Get(
            normalizationContext: ['groups' => ['post:item:read']],
        ),
        new PostMetadata(inputFormats: ['multipart' => ['multipart/form-data']]),
        new Patch(),
        new Delete(),
    ],
    normalizationContext: ['groups' => ['post:read']],
    denormalizationContext: ['groups' => ['post:write']],
    order: ['id' => 'desc'],
    paginationItemsPerPage: 10,
)]
#[ApiResource(
    uriTemplate: '/users/{user_id}/posts.{_format}',
    shortName: 'Post',
    operations: [new GetCollection()],
    uriVariables: [
        'user_id' => new Link(
            fromProperty: 'posts',
            fromClass: User::class,
        ),
    ],
    normalizationContext: ['groups' => ['post:read']],
)]
#[ApiFilter(PropertyFilter::class)]
#[ApiFilter(SearchFilter::class, properties: [
    'owner.username' => 'partial',
])]
class Post
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['post:read', 'user:read', 'post:item:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[ApiFilter(SearchFilter::class, strategy: 'partial')]
    #[Groups(['post:read', 'post:write', 'post:item:read'])]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3, max: 255,
        minMessage: 'Title must have at least {{ limit }} characters',
        maxMessage: 'Title must have at most {{ limit }} characters',
    )]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['post:read', 'post:write', 'post:item:read'])]
    #[Assert\NotBlank]
    #[Assert\Length(min: 10,
        minMessage: 'Content must have at least {{ limit }} characters',
    )]
    private ?string $content = null;

    #[ORM\ManyToOne(inversedBy: 'posts')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['post:read', 'post:write', 'post:item:read'])]
    #[Assert\Valid]
    #[ApiProperty(fetchEager: false)]
    private ?User $owner = null;

    #[ORM\Column]
    #[Groups(['post:read', 'post:write', 'user:read'])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\ManyToOne(targetEntity: MediaObject::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[ApiProperty(types: ['https://schema.org/image'])]
    public ?MediaObject $image = null;

    #[ApiProperty(types: ['https://schema.org/contentUrl'])]
    #[\Symfony\Component\Serializer\Annotation\Groups(['post:read'])]
    public ?string $contentUrl = null;

    #[Vich\UploadableField(mapping: "media_object", fileNameProperty: "filePath")]
    #[\Symfony\Component\Serializer\Annotation\Groups(['post:write'])]
    public ?File $file = null;

    #[ORM\Column(nullable: true)]
    public ?string $filePath = null;

    #[ORM\OneToMany(mappedBy: 'post', targetEntity: Comment::class)]
    #[ApiProperty(types: ['https://schema.org/Comment'])]
    private Collection $comments;

    #[ORM\OneToMany(mappedBy: 'post', targetEntity: Like::class)]
    #[Groups(['post:item:read'])]
    #[ApiProperty(fetchEager: true)]
    private Collection $likes;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->comments = new ArrayCollection();
        $this->likes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): static
    {
        $this->owner = $owner;

        return $this;
    }

    public function getCreatedAt(): string
    {
        return Carbon::instance($this->createdAt)->format('d-m-Y H:i:s');
    }

    #[Groups(['post:read', 'post:item:read'])]
    public function getCreatedAtAgo(): string
    {
        return Carbon::instance($this->createdAt)->diffForHumans();
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): static
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setPost($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): static
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getPost() === $this) {
                $comment->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Like>
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Like $like): static
    {
        if (!$this->likes->contains($like)) {
            $this->likes->add($like);
            $like->setPost($this);
        }

        return $this;
    }

    public function removeLike(Like $like): static
    {
        if ($this->likes->removeElement($like)) {
            // set the owning side to null (unless already changed)
            if ($like->getPost() === $this) {
                $like->setPost(null);
            }
        }

        return $this;
    }

    #[Groups(['post:item:read'])]
    public function getLikesCount(): int
    {
        return $this->likes->count();
    }

    #[Groups(['post:item:read'])]
    public function getLikeId()
    {
        foreach ($this->likes as $like) {
            if ($like->getOwner()->getId() === 1111) {
                return $like->getId();
            }
        }

        return null;
    }
}
