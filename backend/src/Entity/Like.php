<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post as PostMetadata;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Put;
use App\Repository\LikeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: LikeRepository::class)]
#[ORM\Table(name: '`like`')]
//#[UniqueEntity(fields: ['post', 'owner'])]
//#[UniqueEntity(fields: ['comment', 'owner'])]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Get(),
        new PostMetadata(),
        new Delete(),
    ],
    normalizationContext: ['groups' => ['like:read']],
    denormalizationContext: ['groups' => ['like:write']],
    security: "is_granted('ROLE_USER')"
)]
#[ApiResource(
    uriTemplate: '/users/{user_id}/likes.{_format}',
    shortName: 'Like',
    operations: [new GetCollection()],
    uriVariables: [
        'user_id' => new Link(
            fromProperty: 'likes',
            fromClass: User::class,
        ),
    ],
    normalizationContext: ['groups' => ['like:read']],
)]
class Like
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['like:read', 'post:read', 'post:item:read', 'comment:collection:read'])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'likes')]
    #[Groups(['like:read', 'like:write'])]
    private ?Post $post = null;

    #[ORM\ManyToOne(fetch: 'EAGER', inversedBy: 'likes')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['like:read', 'like:write', 'post:item:read', 'comment:collection:read'])]
    #[ApiProperty(fetchEager: false)]
    #[Assert\NotNull]
    private User $owner;

    #[ORM\ManyToOne(inversedBy: 'likes')]
    #[Groups(['like:read', 'like:write'])]
    private ?Comment $comment = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): static
    {
        $this->post = $post;

        return $this;
    }

    public function getOwner(): User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): static
    {
        $this->owner = $owner;

        return $this;
    }

    public function getComment(): ?Comment
    {
        return $this->comment;
    }

    public function setComment(?Comment $comment): static
    {
        $this->comment = $comment;

        return $this;
    }
}
