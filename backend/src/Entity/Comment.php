<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post as PostMetadata;
use ApiPlatform\Metadata\Link;
use App\Repository\CommentRepository;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new PostMetadata(),
        new Patch(),
        new Delete(),
    ],
    normalizationContext: ['groups' => ['comment:read']],
    denormalizationContext: ['groups' => ['comment:write']],
    paginationItemsPerPage: 10,
)]
#[ApiResource(
    uriTemplate: '/posts/{post_id}/comments.{_format}',
    shortName: 'Comment',
    operations: [new GetCollection()],
    uriVariables: [
        'post_id' => new Link(
            fromProperty: 'comments',
            fromClass: Post::class,
        ),
    ],
    normalizationContext: ['groups' => ['comment:read']],
    order: ['id' => 'desc'],
    paginationItemsPerPage: 10,
)]
#[GetCollection(normalizationContext: ['groups' => 'comment:collection:read'])]
class Comment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['comment:collection:read'])]
    private ?int $id = null;

    #[ORM\ManyToOne(fetch: 'EAGER', inversedBy: 'comments')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['comment:collection:read', 'comment:write'])]
    #[ApiProperty(fetchEager: false)]
    private ?User $owner = null;

    #[ORM\ManyToOne(inversedBy: 'comments')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['comment:write'])]
    private ?Post $post = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 10,
        minMessage: 'Content must have at least {{ limit }} characters',
    )]
    #[Groups(['comment:collection:read', 'comment:write', 'post:item:read'])]
    private string $body;

    #[ORM\Column]
    #[Groups(['comment:collection:read', 'user:read'])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\OneToMany(mappedBy: 'comment', targetEntity: Like::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['comment:collection:read'])]
    #[Assert\Unique]
    private Collection $likes;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->likes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): static
    {
        $this->owner = $owner;

        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): static
    {
        $this->post = $post;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): static
    {
        $this->body = $body;

        return $this;
    }

    public function getCreatedAt(): string
    {
        return Carbon::instance($this->createdAt)->format('d-m-Y H:i:s');
    }

    #[Groups(['comment:collection:read'])]
    public function getCreatedAtAgo(): string
    {
        return Carbon::instance($this->createdAt)->diffForHumans();
    }

    /**
     * @return Collection<int, Like>
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Like $like): static
    {
        if (!$this->likes->contains($like)) {
            $this->likes->add($like);
            $like->setComment($this);
        }

        return $this;
    }

    public function removeLike(Like $like): static
    {
        if ($this->likes->removeElement($like)) {
            // set the owning side to null (unless already changed)
            if ($like->getComment() === $this) {
                $like->setComment(null);
            }
        }

        return $this;
    }

    #[Groups(['comment:collection:read'])]
    public function getLikesCount(): int
    {
        return $this->likes->count();
    }

    #[Groups(['comment:collection:read'])]
    public function getLikeId(): ?int
    {
        foreach ($this->likes as $like) {
            if ($like->getOwner()->getId() === 3763) {
               return $like->getId();
            }
        }

        return null;
    }
}
