<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class MyCustomLogoutListener
{
    public function __invoke(LogoutEvent $logoutEvent): void
    {
        $logoutEvent->setResponse(new JsonResponse(['success' => true]));
    }
}
