import { useCallback, useEffect, useRef, useState } from "react";
import axios from "../lib/axios";
import Post from "../components/Post.jsx";

function Index() {
    const [posts, setPosts] = useState([]);
    const [data, setData] = useState({});
    const [page, setPage] = useState(1);
    const [isLoading, setIsLoading] = useState(false)

    const fetchPosts = useCallback(() => {
        setIsLoading(true);
        try {
            axios
                .get("/api/posts", {
                    headers: {
                        'Accept': 'application/ld+json',
                    },
                    params: {
                        page,
                    },
                })
                .then((response) => {
                    setPosts((prevPosts) => [...prevPosts, ...response.data['hydra:member']]);
                    setData(response.data['hydra:view']);
                })
                .finally(() => setIsLoading(false));
        } catch (error) {
            console.log(error);
        }
    }, [page]);

    useEffect(() => {
        fetchPosts();
    }, [fetchPosts, page]);

    const intObserver = useRef();
    const lastPostRef = useCallback(
        (post) => {
            if (intObserver.current) intObserver.current.disconnect();

            intObserver.current = new IntersectionObserver((entries) => {
                if (entries[0].isIntersecting && data["hydra:next"]) {
                    console.log('We are near the last post!');
                    setPage((prevPage) => prevPage + 1);
                }
            });

            if (post) intObserver.current.observe(post);
        },
        [data]
    );

    if (isLoading) {
        return <div className="loading loading-spinner loading-lg"></div>;
    }

    if (posts.length === 0) {
        return (
            <section className="bg-white dark:bg-gray-800">
                <div className="container mx-auto py-10 px-4 md:px-6">
                    <div className="text-center">
                        <h1 className="text-3xl font-bold text-gray-800 dark:text-white">
                            No posts added yet...
                        </h1>
                    </div>
                </div>
            </section>
        );
    }

    return (
        <section className="bg-white dark:bg-gray-800">
            <div className="container mx-auto py-10 px-4 md:px-6">
                <div className="grid grid-cols-1 gap-8 mt-8 md:mt-16 md:grid-cols-2">
                    {posts.map((post, index) => (
                        <Post
                            ref={index === posts.length - 1 ? lastPostRef : null}
                            post={post}
                            key={post.id}
                        />
                    ))}
                </div>
            </div>
        </section>
    );
}

export default Index;
