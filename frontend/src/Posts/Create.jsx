import { useState } from "react";
import axios from "../lib/axios.js";
import { useNavigate } from "react-router-dom";
import Errors from "../components/Errors.jsx";
import {useAuth} from "../hooks/useAuth.js";

function Create() {
    const navigate = useNavigate();
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [selectedFile, setSelectedFile] = useState(null);
    const [errors, setErrors] = useState([]);

    const { user } = useAuth({middleware: 'auth'});

    const handleFileChange = (event) => {
        setSelectedFile(event.target.files[0]);
    };

    async function handleFormSubmit(event) {
        event.preventDefault();

        const formData = new FormData();
        formData.append("file", selectedFile);
        formData.append("title", title);
        formData.append("content", description);
        formData.append("owner", "api/users/" + user.id);

        try {
            setErrors([]);
            await axios.post("/api/posts", formData, {
                headers: {
                    "Content-Type": "multipart/form-data",
                },
            });

            navigate("/posts");
        } catch (error) {
            console.log(error);
            if (error.response.status === 422) {
                setErrors(error.response.data.violations);
            }

            if (!selectedFile) {
                setErrors([{ propertyPath: "file", message: "File is required" }]);
            }
        }
    }

    return (
        <div className="flex items-center justify-center p-12 w-full">
            <div className="mx-auto w-full bg-white dark:bg-gray-700 text-gray-800 dark:text-white">
                <form className="py-6 px-9" onSubmit={handleFormSubmit}>
                    <div className="mb-5">
                        <label
                            htmlFor="title"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Title
                        </label>
                        <input
                            value={title}
                            onChange={(event) => setTitle(event.target.value)}
                            type="text"
                            name="title"
                            id="title"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="title" />
                    </div>

                    <div className="mb-5">
                        <label
                            htmlFor="description"
                            className="mb-3 block text-base font-medium dark:text-white text-[#07074D]"
                        >
                            Description
                        </label>
                        <textarea
                            rows="10"
                            value={description}
                            onChange={(event) => setDescription(event.target.value)}
                            name="description"
                            id="description"
                            className="w-full rounded-md border border-[#e0e0e0] bg-white dark:bg-gray-600 py-3 px-6 text-base font-medium text-[#6B7280] dark:text-white outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                        <Errors errors={errors} field="content" />
                    </div>

                    <div className="mt-4 mb-4">
                        <input
                            type="file"
                            name="file"
                            id="file"
                            onChange={handleFileChange}
                            className="hidden"
                        />
                        <label
                            htmlFor="file"
                            className="cursor-pointer bg-blue-600 text-white py-2 px-4 rounded-md hover:bg-blue-700"
                        >
                            Choose a Photo
                        </label>

                        {selectedFile && (
                            <div className="mt-4">
                                <p className="text-lg font-semibold">Selected Photo:</p>
                                <img
                                    src={URL.createObjectURL(selectedFile)}
                                    alt="Selected"
                                    className="mt-2 rounded-md"
                                    style={{ maxWidth: "100%", maxHeight: "200px" }}
                                />
                            </div>
                        )}
                    </div>
                    <Errors errors={errors} field="file" />

                    <div>
                        <button
                            className="hover:shadow-form w-full rounded-md bg-purple-600 hover:bg-purple-700 py-3 px-8 text-center text-base font-semibold text-white outline-none"
                            type="submit"
                        >
                            Add post
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Create;
