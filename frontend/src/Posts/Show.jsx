import { useState, useRef, useEffect, useCallback } from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import axios from '../lib/axios.js';
import Comment from '../components/Comment.jsx';
import Errors from '../components/Errors.jsx';
import {useAuth} from "../hooks/useAuth.js";

function Show() {
    const { id } = useParams();
    const [post, setPost] = useState({});
    const [comments, setComments] = useState([]);
    const [data, setData] = useState([]);
    const [url, setUrl] = useState(`/api/posts/${id}/comments`);
    const [body, setBody] = useState('');
    const [errors, setErrors] = useState([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isPostLikesModalOpen, setIsPostLikesModalOpen] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [isFetching, setIsFetching] = useState(false);
    const modalRef = useRef(null);
    const postLikesModalRef = useRef(null);
    const navigate = useNavigate();

    const { user, isFetchingUser } = useAuth();

    useEffect(() => {
        const handleModalClick = (event) => {
            if (modalRef.current && !modalRef.current.contains(event.target)) {
                closeModal();
            }
        };

        if (isModalOpen) {
            document.addEventListener('mousedown', handleModalClick);
        } else {
            document.removeEventListener('mousedown', handleModalClick);
        }

        return () => {
            document.removeEventListener('mousedown', handleModalClick);
        };
    }, [isModalOpen]);

    useEffect(() => {
        const handleModalClick = (event) => {
            if (postLikesModalRef.current && !postLikesModalRef.current.contains(event.target)) {
                closePostLikesModal();
            }
        };

        if (isPostLikesModalOpen) {
            document.addEventListener('mousedown', handleModalClick);
        } else {
            document.removeEventListener('mousedown', handleModalClick);
        }

        return () => {
            document.removeEventListener('mousedown', handleModalClick);
        };
    }, [isPostLikesModalOpen]);

    const fetchPost = useCallback((initial = true) => {
        if (initial) {
            setIsFetching(true);
        }

        axios
            .get(`/api/posts/${id}`)
            .then((response) => setPost(response.data))
            .finally(() => setIsFetching(false));
    }, [id]);

    const fetchComments = useCallback(() => {
        axios
            .get(url, {
                headers: {
                    Accept: 'application/ld+json',
                },
            })
            .then((response) => {
                setComments(response.data['hydra:member']);
                setData(response.data['hydra:view']);
            })
    }, [url]);

    useEffect(() => {
        if (!isFetchingUser) {
            fetchPost();
        }
    }, [fetchPost, isFetchingUser, user]);

    useEffect(() => {
        if (!isFetching) {
            fetchComments();
        }
    }, [fetchComments, isFetching, url]);

    if (isFetching) {
        return <div className="loading loading-spinner loading-lg"></div>;
    }

    function handlePageChange(event) {
        if (event === 'prev') {
            setUrl(data['hydra:previous']);
        }

        if (event === 'next') {
            setUrl(data['hydra:next']);
        }
    }
    
    async function handleAddComment() {
        if (!body) {
            return;
        }

        if (!user) {
            return;
        }

        setBody('');

        try {
            await axios.post('/api/comments', {
                owner: '/api/users/' + user.id,
                post: `/api/posts/${id}`,
                body,
            }).then(() => {
                fetchComments();
                closeModal();
            });
        } catch (error) {
            if (error.response.status === 422) {
                setErrors(error.response.data.violations);
            }
        }
    }

    const openModal = () => {
        if (!user) {
            navigate('/login');
        }

        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
        setBody('');
        setErrors([]);
    };

    const openPostLikesModal = () => {
        setIsPostLikesModalOpen(true);
    };

    const closePostLikesModal = () => {
        setIsPostLikesModalOpen(false);
    };

    const handleLikeClick = async () => {
        if (isLoading) {
            return;
        }

        if (!user) {
            navigate('/login');
        }

        try {
            setIsLoading(true);
            const res = await axios.post('/api/likes', {
                owner: '/api/users/' + user.id,
                post: `/api/posts/${id}`,
            })

            if (res.status === 201) {
                fetchPost(false);
            }
        } catch (error) {
            console.log(error);
        } finally {
            setIsLoading(false);
        }
    }

    const handleDislikeClick = async () => {
        if (isLoading) {
            return;
        }

        try {
            setIsLoading(true);
            const res = await axios.delete('/api/likes/' + post.likeId);

            if (res.status === 204) {
                fetchPost(false);
            }
        } catch (error) {
            console.log(error);
        } finally {
            setIsLoading(false);
        }
    }

    post.likes?.map((like) => {
        if (like.owner.id === user?.id) {
            post.likeId = like.id;
        }
    })

    return (
        <div className="w-full p-4 h-fit bg-gray-100 dark:bg-gray-800 text-gray-800 dark:text-white">
            <div className="text-center">
                <div className="font-bold text-purple-600">{post.title}</div>
                <div className="text-gray-600">{post.content}</div>
                <div className="mt-8 text-gray-600">{post.createdAtAgo}</div>
                <div className="text-end text-purple-600">{post.owner?.username}</div>
                <div className="flex justify-start items-center space-x-1">
                    {post.likeId
                        ? <button onClick={handleDislikeClick}
                                  className="text-purple-600 hover:text-purple-700 dark:text-purple-400 hover:dark:text-purple-500">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"
                                 className="w-6 h-6">
                                <path
                                    d="M7.493 18.75c-.425 0-.82-.236-.975-.632A7.48 7.48 0 016 15.375c0-1.75.599-3.358 1.602-4.634.151-.192.373-.309.6-.397.473-.183.89-.514 1.212-.924a9.042 9.042 0 012.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 00.322-1.672V3a.75.75 0 01.75-.75 2.25 2.25 0 012.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 01-2.649 7.521c-.388.482-.987.729-1.605.729H14.23c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 00-1.423-.23h-.777zM2.331 10.977a11.969 11.969 0 00-.831 4.398 12 12 0 00.52 3.507c.26.85 1.084 1.368 1.973 1.368H4.9c.445 0 .72-.498.523-.898a8.963 8.963 0 01-.924-3.977c0-1.708.476-3.305 1.302-4.666.245-.403-.028-.959-.5-.959H4.25c-.832 0-1.612.453-1.918 1.227z"/>
                            </svg>
                        </button>
                        : <button onClick={handleLikeClick}
                                  className="text-purple-600 hover:text-purple-700 dark:text-purple-400 hover:dark:text-purple-500">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5"
                                 stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                      d="M6.633 10.5c.806 0 1.533-.446 2.031-1.08a9.041 9.041 0 012.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 00.322-1.672V3a.75.75 0 01.75-.75A2.25 2.25 0 0116.5 4.5c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 01-2.649 7.521c-.388.482-.987.729-1.605.729H13.48c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 00-1.423-.23H5.904M14.25 9h2.25M5.904 18.75c.083.205.173.405.27.602.197.4-.078.898-.523.898h-.908c-.889 0-1.713-.518-1.972-1.368a12 12 0 01-.521-3.507c0-1.553.295-3.036.831-4.398C3.387 10.203 4.167 9.75 5 9.75h1.053c.472 0 .745.556.5.96a8.958 8.958 0 00-1.302 4.665c0 1.194.232 2.333.654 3.375z"/>
                            </svg>
                        </button>
                    }
                    <div>
                        <button disabled={!post.likesCount} onClick={openPostLikesModal}>{post.likesCount}</button>
                    </div>
                </div>
            </div>

            <hr className="mt-12 mb-10"/>
            <div className="flex justify-end items-center">
                <button onClick={openModal} className="btn btn-warning btn-outline dark:text-gray-100">
                    Add Comment
                </button>
            </div>

            {!comments.length ? (
                <div className="mt-5 text-gray-600">No comments added yet...</div>
            ) : (
                <div className="pt-8">
                    <div>
                        {comments.map((comment) => (
                            <Comment callback={fetchComments} key={comment.id} comment={comment}/>
                        ))}
                    </div>
                    <div className="flex justify-center">
                        <button
                            onClick={() => handlePageChange('prev')}
                            className={`btn btn-outline btn-info ${
                                !(data && data['hydra:previous']) ? 'opacity-50 cursor-not-allowed' : ''
                            }`}
                            disabled={!(data && data['hydra:previous'])}
                        >
                            Previous
                        </button>
                        <button
                            onClick={() => handlePageChange('next')}
                            className={`btn btn-outline btn-accent ms-2 ${
                                !(data && data['hydra:next']) ? 'opacity-50 cursor-not-allowed' : ''
                            }`}
                            disabled={!(data && data['hydra:next'])}
                        >
                            Next
                        </button>
                    </div>
                </div>
            )}

            <div>
                {isModalOpen && (
                    <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center">
                        <div
                            ref={modalRef}
                            className="bg-gray-100 dark:bg-gray-700 p-8 rounded-md w-full max-w-md"
                        >
                            <h2 className="text-2xl font-bold mb-4">Add Comment</h2>
                            <textarea
                                value={body}
                                onChange={(event) => setBody(event.target.value)}
                                rows="6"
                                name="body"
                                id="body"
                                className="w-full rounded-md border border-gray-300 dark:border-gray-600 bg-white dark:bg-gray-600 py-2 px-3 text-base font-medium text-gray-600 dark:text-white outline-none focus:border-purple-600 focus:shadow-md"
                            />
                            <Errors errors={errors} field="body"/>
                            <div className="flex justify-end mt-3">
                                <button onClick={handleAddComment} className="btn btn-success dark:text-gray-100">
                                    Add Comment
                                </button>
                                <button onClick={closeModal} className="btn btn-error ms-2 dark:text-gray-100">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                )}
                {isPostLikesModalOpen && (
                        <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center">
                            <div
                                ref={postLikesModalRef}
                                className="bg-gray-100 dark:bg-gray-700 p-8 rounded-md w-full max-w-md"
                            >
                                <div className="flex justify-end">
                                    <button onClick={closePostLikesModal}>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                             strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                                            <path strokeLinecap="round" strokeLinejoin="round"
                                                  d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                        </svg>
                                    </button>
                                </div>

                                {post.likesCount > 0 && post.likes.map((like) => (
                                    <div key={like.id}>{like.owner.username}</div>
                                ))}
                            </div>
                        </div>
                )}
            </div>
        </div>
    );
}

export default Show;
