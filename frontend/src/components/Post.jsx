import {Link} from "react-router-dom";
import React from "react";

const Post = React.forwardRef(({ post }, ref) => {
    const postBody = (
        <div className="lg:flex">
            <img className="object-cover w-full h-56 rounded-lg lg:w-64"
                 src="https://images.unsplash.com/photo-1497032628192-86f99bcd76bc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80"
                 alt=""
            />
            <div className="flex flex-col justify-between py-6 lg:mx-6">
                <Link to={`/posts/${post.id}`} className="text-xl font-semibold hover:underline" title={post.title}>
                    {post.title}
                </Link>
                <span className="text-sm">{post.createdAtAgo}</span>
            </div>
        </div>
    )

    return ref
        ? <article ref={ref}>{postBody}</article>
        : <article>{postBody}</article>
})

export default Post
