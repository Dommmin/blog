import {useState} from 'react';
import axios from "../lib/axios";
import {useNavigate, useParams} from "react-router-dom";
import {useAuth} from "../hooks/useAuth.js";

function Comment({ comment, callback }) {
    const { id } = useParams();
    const [isEditing, setIsEditing] = useState(false);
    const [editedBody, setEditedBody] = useState(comment.body);
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();
    const username = comment?.owner?.username || '';

    const { user } = useAuth();

    const handleEditClick = () => {
        setIsEditing(true);
    };

    const handleSaveEdit = async () => {
        if (editedBody.length < 10) {
            return;
        }

        setIsEditing(false);

        try {
            const res = await axios.patch('/api/comments/' + comment.id, {
                owner: '/api/users/' + comment.owner.id,
                body: editedBody,
                post: '/api/posts/' + id
            }, {
                headers: {
                    'Content-Type': 'application/merge-patch+json'
                }
            })

            console.log(res);

            if (res.status === 200) {
                await callback();
            }
        } catch (e) {
            console.log(e);
        }

    };

    const handleCancelEdit = () => {
        setIsEditing(false);
        setEditedBody(comment.body);
    };

    const handleDeleteClick = async () => {
        try {
            const res = await axios.delete(`/api/comments/${comment.id}`);
            if (res.status === 204) {
                await callback();
            }
        } catch (e) {
            console.log(e);
        }
    };

    const handleLikeClick = async () => {
        if (isLoading) {
            return;
        }

        if (!user) {
            navigate('/login');
        }

        try {
            const res = await axios.post('/api/likes', {
                owner: '/api/users/' + user.id,
                comment: `/api/comments/${comment.id}`,
            })

            if (res.status === 201) {
                await callback();
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleDislikeClick = async () => {
        if (isLoading) {
            return;
        }

        try {
            // setIsLoading(true);
            const res = await axios.delete('/api/likes/' + comment.likeId);

            if (res.status === 204) {
                await callback();
            }
        } catch (error) {
            console.log(error);
        } finally {
            // setIsLoading(false);
        }
    }

    comment.likes?.map((like) => {
        if (like.owner.id === user?.id) {
            comment.likeId = like.id;
        }
    })

    return (
        <div className="mb-6 border p-4 rounded-md bg-white dark:bg-gray-800 shadow-md">
            <div className="flex justify-between items-center mb-2">
                <div className="text-gray-600 dark:text-gray-400"><span className="font-bold text-lg text-emerald-700">{username}</span> &#8226; {comment.createdAtAgo}</div>
                <div className="flex gap-2">
                    {comment.owner.id === user?.id  && <div>
                        <button onClick={handleEditClick}
                                className="text-blue-600 hover:text-blue-700 dark:text-blue-400 dark:hover:text-blue-500">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5"
                                 stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                      d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"/>
                            </svg>
                        </button>
                        <button onClick={handleDeleteClick}
                                className="text-red-600 hover:text-red-700 dark:text-red-400 dark:hover:text-red-500">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5"
                                 stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12"/>
                            </svg>
                        </button>
                    </div>
                    }
                </div>
            </div>

            {isEditing ? (
                <div className="mb-2">
                    <textarea
                        value={editedBody}
                        onChange={(e) => setEditedBody(e.target.value)}
                        rows="3"
                        className="w-full rounded-md border border-gray-300 dark:border-gray-600 bg-white dark:bg-gray-700 py-2 px-3 text-base text-gray-600 dark:text-white outline-none focus:border-purple-600 focus:shadow-md"
                    />
                    <div className="flex justify-end mt-2">
                        <button onClick={handleSaveEdit} className="btn btn-success text-gray-100">
                            Save
                        </button>
                        <button onClick={handleCancelEdit} className="btn btn-error text-gray-100 ms-2">
                            Cancel
                        </button>
                    </div>
                </div>
            ) : (
                <div>{editedBody}</div>
            )}
            <div className="flex justify-start items-center space-x-1 mt-2">
                {comment.likeId
                    ? <button onClick={handleDislikeClick}
                              className="text-purple-600 hover:text-purple-700 dark:text-purple-400 hover:dark:text-purple-500">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"
                             className="w-6 h-6">
                            <path
                                d="M7.493 18.75c-.425 0-.82-.236-.975-.632A7.48 7.48 0 016 15.375c0-1.75.599-3.358 1.602-4.634.151-.192.373-.309.6-.397.473-.183.89-.514 1.212-.924a9.042 9.042 0 012.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 00.322-1.672V3a.75.75 0 01.75-.75 2.25 2.25 0 012.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 01-2.649 7.521c-.388.482-.987.729-1.605.729H14.23c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 00-1.423-.23h-.777zM2.331 10.977a11.969 11.969 0 00-.831 4.398 12 12 0 00.52 3.507c.26.85 1.084 1.368 1.973 1.368H4.9c.445 0 .72-.498.523-.898a8.963 8.963 0 01-.924-3.977c0-1.708.476-3.305 1.302-4.666.245-.403-.028-.959-.5-.959H4.25c-.832 0-1.612.453-1.918 1.227z"/>
                        </svg>
                    </button>
                    : <button onClick={handleLikeClick}
                              className="text-purple-600 hover:text-purple-700 dark:text-purple-400 hover:dark:text-purple-500">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                             strokeWidth="1.5"
                             stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                  d="M6.633 10.5c.806 0 1.533-.446 2.031-1.08a9.041 9.041 0 012.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 00.322-1.672V3a.75.75 0 01.75-.75A2.25 2.25 0 0116.5 4.5c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 01-2.649 7.521c-.388.482-.987.729-1.605.729H13.48c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 00-1.423-.23H5.904M14.25 9h2.25M5.904 18.75c.083.205.173.405.27.602.197.4-.078.898-.523.898h-.908c-.889 0-1.713-.518-1.972-1.368a12 12 0 01-.521-3.507c0-1.553.295-3.036.831-4.398C3.387 10.203 4.167 9.75 5 9.75h1.053c.472 0 .745.556.5.96a8.958 8.958 0 00-1.302 4.665c0 1.194.232 2.333.654 3.375z"/>
                        </svg>
                    </button>
                }
                <div className="text-sm">{comment.likesCount}</div>
            </div>
        </div>
    );
}

export default Comment;
