import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Layout from "./components/Layout.jsx";
import PostsIndex from "./Posts/Index.jsx";
import PostsCreate from "./Posts/Create.jsx";
import PostsShow from "./Posts/Show.jsx";
import Login from "./Auth/Login.jsx";
import Register from "./Auth/Register.jsx";

function App() {
    const router = createBrowserRouter([
        {
            path: "/",
            element: <Layout />,
            children: [
                {
                    path: "/posts",
                    element: <PostsIndex />
                },
                {
                    path: "/posts/create",
                    element: <PostsCreate />
                },
                {
                    path: "/posts/:id",
                    element: <PostsShow />
                }
            ]
        },
        {
            path: "/login",
            element: <Login />
        },
        {
            path: "/register",
            element: <Register />
        }
    ])

  return (
      <RouterProvider router={router} />
  )
}

export default App
