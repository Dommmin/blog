import {Link} from "react-router-dom";
import {useState} from "react";
import {useAuth} from "../hooks/useAuth.js";

function Login() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [shouldRemember, setShouldRemember] = useState(false)
    const [error, setError] = useState('')
    const [isLoading, setIsLoading] = useState(false);

    const { login } = useAuth({
        middleware: 'guest',
        redirectIfAuthenticated: '/posts',
    })

    async function handleFormSubmit(event) {
        event.preventDefault();

        await login({
            email,
            password,
            // remember: shouldRemember,
            setError,
        })
    }

    console.log(error);

    return (
        <div className="min-h-screen flex flex-col items-center justify-center bg-gray-900 text-white">
            {error &&
                <div role="alert" className="max-w-sm alert alert-error mb-4">
                    <svg xmlns="http://www.w3.org/2000/svg" className="stroke-current shrink-0 h-6 w-6" fill="none"
                         viewBox="0 0 24 24">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    </svg>
                    <span>{error}</span>
                </div>
            }
            <div
                className="flex flex-col bg-gray-800 shadow-md px-4 sm:px-6 md:px-8 lg:px-10 py-8 rounded-3xl w-50 max-w-md w-full">
                <div className="font-medium self-center text-xl sm:text-3xl">
                    Login
                </div>
                <div className="mt-10">
                    <form onSubmit={handleFormSubmit}>
                        <div className="flex flex-col mb-5">
                            <label
                                htmlFor="email"
                                className="mb-1 text-xs tracking-wide text-gray-600"
                            >E-Mail Address:</label
                            >
                            <div className="relative">
                                <div
                                    className="inline-flex items-center justify-center absolute left-0 top-0 h-full w-10 text-gray-400"
                                >
                                    <i className="fas fa-at text-blue-500"></i>
                                </div>

                                <input
                                    id="email"
                                    type="email"
                                    name="email"
                                    className="text-sm placeholder-gray-500 pl-10 pr-4 rounded-2xl border border-gray-400 w-full py-2 focus:outline-none focus:border-blue-400"
                                    autoComplete="true"
                                    value={email}
                                    onChange={() => setEmail(event.target.value)}
                                />
                            </div>
                        </div>
                        <div className="flex flex-col mb-6">
                            <label
                                htmlFor="password"
                                className="mb-1 text-xs sm:text-sm tracking-wide text-gray-600"
                            >Password:</label
                            >
                            <div className="relative">
                                <div
                                    className="inline-flex items-center justify-center absolute left-0 top-0 h-full w-10 text-gray-400">
                                  <span>
                                    <i className="fas fa-lock text-blue-500"></i>
                                  </span>
                                </div>
                                <input
                                    id="password"
                                    type="password"
                                    name="password"
                                    className="text-sm placeholder-gray-500 pl-10 pr-4 rounded-2xl border border-gray-400 w-full py-2 focus:outline-none focus:border-blue-400"
                                    autoComplete="true"
                                    value={password}
                                    onChange={() => setPassword(event.target.value)}
                                />
                            </div>
                        </div>
                        <div>
                            <div className="flex justify-between items-center mb-6">
                                <div className="flex items-center">
                                    <input
                                        id="remember_me"
                                        type="checkbox"
                                        name="remember"
                                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500"
                                        onChange={() => setShouldRemember(event.target.checked)}
                                    />
                                    <label
                                        htmlFor="remember_me"
                                        className="ml-2 block text-sm"
                                    >
                                        Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="flex w-full">
                            <button
                                type="submit"
                                className={`flex mt-2 items-center justify-center focus:outline-none text-white text-sm sm:text-base bg-blue-500 hover:bg-blue-600 rounded-2xl py-2 w-full transition duration-150 ease-in ${isLoading ? "opacity-50 cursor-not-allowed" : ""}`}
                            >
                                <span className="mr-2 uppercase">Sign In</span>
                                <span>
                                  <svg
                                      className="h-6 w-6"
                                      fill="none"
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                      strokeWidth="2"
                                      viewBox="0 0 24 24"
                                      stroke="currentColor"
                                  >
                                    <path
                                        d="M13 9l3 3m0 0l-3 3m3-3H8m13 0a9 9 0 11-18 0 9 9 0 0118 0z"
                                    />
                                  </svg>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div className="flex justify-center items-center mt-6">
                Don't have an account yet?
                <Link to="/register" className="text-xs ml-2 text-blue-500 font-semibold">Register here</Link>
            </div>
        </div>
    );
}

export default Login
