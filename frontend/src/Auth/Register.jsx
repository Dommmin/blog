import {Link} from "react-router-dom";
import {useState} from "react";
import {useAuth} from "../hooks/useAuth.js";
import Errors from "../components/Errors.jsx";

function Register() {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [agreeTerms, setAgreeTerms] = useState(false)
    const [errors, setErrors] = useState([])
    const [isLoading, setIsLoading] = useState(false);

    const { register } = useAuth({
        middleware: 'guest',
        redirectIfAuthenticated: '/posts',
    })

    const handleFormSubmit = async (event) => {
        event.preventDefault()
        setIsLoading(true);

        await register({
            username: name,
            email,
            plainPassword: password,
            agreeTerms,
            setErrors,
        })

        setIsLoading(false);
    }

    return (
        <div className="min-h-screen flex flex-col items-center justify-center bg-gray-900 text-white">
            <div className="flex flex-col bg-gray-800 shadow-md px-4 sm:px-6 md:px-8 lg:px-10 py-8 rounded-3xl w-50 max-w-md w-full">
                <div className="font-medium self-center text-xl sm:text-3xl">
                    Register
                </div>

                <div className="mt-10">
                    <form onSubmit={handleFormSubmit} autoComplete="off">
                        <div className="flex flex-col mb-5">
                            <label htmlFor="name" className="mb-1 text-xs tracking-wide text-gray-600">Name:</label>
                            <div className="relative">
                                <div
                                    className="inline-flex items-center justify-center absolute left-0 top-0 h-full w-10 text-gray-400">
                                    <i className="fas fa-user text-blue-500"></i>
                                </div>
                                <input
                                    id="name"
                                    type="text"
                                    name="name"
                                    className=" text-sm placeholder-gray-500 pl-10 pr-4 rounded-2xl border border-gray-400 w-full py-2 focus:outline-none focus:border-blue-400"
                                    value={name}
                                    onChange={() => setName(event.target.value)}
                                    required
                                />
                            </div>
                            <div className="mt-2">
                                <Errors errors={errors} field="username"/>
                            </div>
                        </div>
                        <div className="flex flex-col mb-5">
                            <label
                                htmlFor="email"
                                className="mb-1 text-xs tracking-wide text-gray-600"
                            >E-Mail Address:</label
                            >
                            <div className="relative">
                                <div
                                    className="inline-flex items-center justify-center absolute left-0 top-0 h-full w-10 text-gray-400"
                                >
                                    <i className="fas fa-at text-blue-500"></i>
                                </div>

                                <input
                                    id="email"
                                    type="email"
                                    name="email"
                                    className="text-sm placeholder-gray-500 pl-10 pr-4 rounded-2xl border border-gray-400 w-full py-2 focus:outline-none focus:border-blue-400"
                                    autoComplete="off"
                                    value={email}
                                    onChange={() => setEmail(event.target.value)}
                                    required
                                />
                            </div>
                            <div className="mt-2">
                                <Errors errors={errors} field="email"/>
                            </div>
                        </div>
                        <div className="flex flex-col mb-6">
                            <label
                                htmlFor="password"
                                className="mb-1 text-xs sm:text-sm tracking-wide text-gray-600"
                            >Password:</label
                            >
                            <div className="relative">
                                <div
                                    className="inline-flex items-center justify-center absolute left-0 top-0 h-full w-10 text-gray-400">
                                  <span>
                                    <i className="fas fa-lock text-blue-500"></i>
                                  </span>
                                </div>
                                <input
                                    id="password"
                                    type="password"
                                    name="password"
                                    className="text-sm placeholder-gray-500 pl-10 pr-4 rounded-2xl border border-gray-400 w-full py-2 focus:outline-none focus:border-blue-400"
                                    autoComplete="off"
                                    value={password}
                                    onChange={() => setPassword(event.target.value)}
                                    required
                                />
                            </div>
                            <div className="mt-2">
                                <Errors errors={errors} field="plainPassword"/>
                            </div>
                        </div>
                        <div className="flex  flex-col mb-6">
                            <div className="space-x-2 justify-start items-center">
                                <input
                                    type="checkbox"
                                    id="agreeTerms"
                                    value={agreeTerms}
                                    onChange={() => setAgreeTerms(!agreeTerms)}
                                    name="agreeTerms"
                                    className="rounded-xl"
                                    required
                                />
                                <label htmlFor="agreeTerms">Agree terms</label>
                            </div>
                            <div className="mt-2">
                                <Errors errors={errors} field="agreeTerms"/>
                            </div>
                        </div>

                        <div className="flex w-full">
                            <button
                                disabled={isLoading}
                                type="submit"
                                className={`flex mt-2 items-center justify-center focus:outline-none text-white text-sm sm:text-base bg-blue-500 hover:bg-blue-600 rounded-2xl py-2 w-full transition duration-150 ease-in ${isLoading
                                ? 'cursor-not-allowed opacity-50' : ''}`}
                            >
                                <span className="mr-2 uppercase">Sign Up</span>
                                <span>
                              <svg
                                  className="h-6 w-6"
                                  fill="none"
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  strokeWidth="2"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                              >
                                <path
                                    d="M13 9l3 3m0 0l-3 3m3-3H8m13 0a9 9 0 11-18 0 9 9 0 0118 0z"
                                />
                              </svg>
                            </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div className="flex justify-center items-center mt-6">
                Already registered?
                <Link to="/login" className="ml-2 text-blue-500 font-semibold">Login here</Link>
            </div>
        </div>
    );
}

export default Register
